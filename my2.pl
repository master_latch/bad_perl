#!/usr/bin/perl

bar();

print "after bar: $x\n";

sub bar {
    $x = 10;
    print "in bar pre foo(): $x\n";
    foo();
    print "in bar post foo(): $x\n";
}

sub foo {
    print "in foo pre-set: $x\n";
    $x = 5;
    print "in foo post-set: $x\n";
}

my $x = 3;
print "last: $x\n";
